import{_ as n}from"./plugin-vue_export-helper-c27b6911.js";import{o as s,c as a,b as e}from"./app-af62e8e7.js";const p={},t=e(`<h1 id="配置-proxyserver" tabindex="-1"><a class="header-anchor" href="#配置-proxyserver" aria-hidden="true">#</a> 配置 proxyServer</h1><p><code>api</code>写法</p><div class="language-typescript line-numbers-mode" data-ext="ts"><pre class="language-typescript"><code><span class="token keyword">function</span> <span class="token function">getWeather</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
  <span class="token keyword">return</span> http<span class="token punctuation">.</span><span class="token function">request</span><span class="token punctuation">(</span><span class="token punctuation">{</span>
    url<span class="token operator">:</span> <span class="token string">&#39;/weacher/v3/weather/weatherInfo&#39;</span><span class="token punctuation">,</span>
    baseURL<span class="token operator">:</span> <span class="token string">&#39;&#39;</span><span class="token punctuation">,</span> <span class="token comment">// 这很重要</span>
    headers<span class="token operator">:</span> <span class="token punctuation">{</span>
      isNotToken<span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    params<span class="token operator">:</span> <span class="token punctuation">{</span>
      key<span class="token operator">:</span> <span class="token string">&#39;70e54be373b3339ebee409cca2a4e4b1&#39;</span><span class="token punctuation">,</span>
      city<span class="token operator">:</span> <span class="token number">130209</span><span class="token punctuation">,</span>
      extensions<span class="token operator">:</span> <span class="token string">&#39;base&#39;</span><span class="token punctuation">,</span>
      output<span class="token operator">:</span> <span class="token string">&#39;json&#39;</span><span class="token punctuation">,</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
  <span class="token punctuation">}</span><span class="token punctuation">)</span> <span class="token keyword">as</span> <span class="token builtin">Promise</span><span class="token operator">&lt;</span>WeacherApi<span class="token punctuation">.</span>WeacherRes<span class="token operator">&gt;</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span>

<span class="token keyword">export</span> <span class="token keyword">default</span> <span class="token punctuation">{</span> getWeather <span class="token punctuation">}</span><span class="token punctuation">;</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p><code>vite.config.ts</code>配置</p><div class="language-javascript line-numbers-mode" data-ext="js"><pre class="language-javascript"><code><span class="token literal-property property">server</span><span class="token operator">:</span> <span class="token punctuation">{</span>
      <span class="token literal-property property">port</span><span class="token operator">:</span> <span class="token number">5173</span><span class="token punctuation">,</span>
      <span class="token literal-property property">strictPort</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span> <span class="token comment">// 严格端口 true:如果端口已被使用，则直接退出，而不会再进行后续端口的尝试。</span>
      <span class="token doc-comment comment">/**
       * <span class="token keyword">@description</span> 解决chrome设置origin:*也跨域机制,代理/api前缀到服务基地址
       * 最终的地址会将axios设置的baseUrl:/dev代理拼接成[target][/dev][/xxx/yyy]
       */</span>
      <span class="token literal-property property">proxy</span><span class="token operator">:</span> <span class="token punctuation">{</span>
        <span class="token string-property property">&#39;/weacher&#39;</span><span class="token operator">:</span> <span class="token punctuation">{</span>
          <span class="token literal-property property">target</span><span class="token operator">:</span> <span class="token string">&#39;https://restapi.amap.com&#39;</span><span class="token punctuation">,</span> <span class="token comment">// 接口基地址</span>
          <span class="token function-variable function">rewrite</span><span class="token operator">:</span> <span class="token parameter">path</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
            console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span>path<span class="token punctuation">)</span><span class="token punctuation">;</span> <span class="token comment">// 打印[/dev/xxx/yyy] 这就是http-proxy要请求的url,如果服务器真实地址是没有/dev前缀的话要replace掉,如果有可以不replace</span>
            <span class="token keyword">return</span> path<span class="token punctuation">.</span><span class="token function">replace</span><span class="token punctuation">(</span><span class="token regex"><span class="token regex-delimiter">/</span><span class="token regex-source language-regex">^\\/weacher</span><span class="token regex-delimiter">/</span></span><span class="token punctuation">,</span> <span class="token string">&#39;&#39;</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
          <span class="token punctuation">}</span>
        <span class="token punctuation">}</span>
      <span class="token punctuation">}</span>
    <span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,5),o=[t];function c(i,l){return s(),a("div",null,o)}const d=n(p,[["render",c],["__file","vite-proxy-server.html.vue"]]);export{d as default};
