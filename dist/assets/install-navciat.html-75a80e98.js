import{_ as n}from"./plugin-vue_export-helper-c27b6911.js";import{o as s,c as a,b as e}from"./app-af62e8e7.js";const i={},t=e(`<h1 id="linux-安装navicat16-premium-cs-并破解" tabindex="-1"><a class="header-anchor" href="#linux-安装navicat16-premium-cs-并破解" aria-hidden="true">#</a> linux 安装navicat16-premium-cs 并破解</h1><h3 id="破解navicat16-使用-navicat-keygen-for-linux" tabindex="-1"><a class="header-anchor" href="#破解navicat16-使用-navicat-keygen-for-linux" aria-hidden="true">#</a> 破解navicat16 使用 navicat-keygen for linux</h3><ol><li>从，你应该会得到一个AppImage文件，例如 navicat16-premium-cs.AppImage。</li><li>提取安装包内容<br> 我假定这个AppImage文件在 ~/ 文件夹下。<br> 提取 navicat16-premium-cs.AppImage 里的所有文件到一个文件夹，例如 ~/navicat16-premium-cs-patched:</li></ol><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token function">mkdir</span> ~/navicat16-premium-cs
<span class="token function">sudo</span> <span class="token function">mount</span> <span class="token parameter variable">-o</span> loop ~/navicat16-premium-cs.AppImage ~/navicat16-premium-cs
<span class="token function">cp</span> <span class="token parameter variable">-r</span> ~/navicat16-premium-cs ~/navicat16-premium-cs-patched
<span class="token function">sudo</span> <span class="token function">umount</span> ~/navicat16-premium-cs
<span class="token function">rm</span> <span class="token parameter variable">-rf</span> ~/navicat16-premium-cs
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ol start="2"><li><p>编译patcher和keygen。<br> pacman -S cmake repidjson</p><p>clone 仓库</p><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token function">git</span> clone <span class="token parameter variable">-b</span> linux --single-branch https://notabug.org/doublesine/navicat-keygen.git
<span class="token builtin class-name">cd</span> navicat-keygen
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div><p>编译</p><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token function">mkdir</span> build <span class="token operator">&amp;&amp;</span> <span class="token builtin class-name">cd</span> build
cmake <span class="token parameter variable">-DCAME_BUILD_TYPE</span><span class="token operator">=</span>Release <span class="token punctuation">..</span>
cmake <span class="token parameter variable">--build</span> <span class="token builtin class-name">.</span> -- <span class="token parameter variable">-j8</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div></li><li><p>使用 navicat-patcher 替换官方公钥。</p></li></ol><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code>Usage:
  navicat-patcher <span class="token punctuation">[</span>OPTION<span class="token punctuation">..</span>.<span class="token punctuation">]</span> <span class="token operator">&lt;</span>navicat root path<span class="token operator">&gt;</span> <span class="token punctuation">[</span>RSA-2048 private key file<span class="token punctuation">]</span>

--dry-run  Run patcher without applying any patches
  -h, <span class="token parameter variable">--help</span>  Print <span class="token builtin class-name">help</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>例如：</p><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code>./navicat-patcher ~/navicat16-premium-cs-patched
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><p>Navicat Premium 16.0.7 简体中文版本 for Linux 已经通过测试，下面是一份样例输出：</p><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code>***************************************************
* navicat-patcher by @DoubleLabyrinth *
*version: <span class="token number">16.0</span>.7.0  *
***************************************************

<span class="token punctuation">[</span>+<span class="token punctuation">]</span> Try to <span class="token function">open</span> libcc.dll <span class="token punctuation">..</span>. OK<span class="token operator">!</span>

<span class="token punctuation">[</span>*<span class="token punctuation">]</span> patch_solution_since<span class="token operator">&lt;</span><span class="token number">16</span>, <span class="token number">0</span>, <span class="token number">7</span>, <span class="token operator"><span class="token file-descriptor important">0</span>&gt;</span>: m_va_CSRegistrationInfoFetcher_LINUX_vtable <span class="token operator">=</span> 0x0000000002fbf038
<span class="token punctuation">[</span>*<span class="token punctuation">]</span> patch_solution_since<span class="token operator">&lt;</span><span class="token number">16</span>, <span class="token number">0</span>, <span class="token number">7</span>, <span class="token operator"><span class="token file-descriptor important">0</span>&gt;</span>: m_va_CSRegistrationInfoFetcher_LINUX_GenerateRegistrationKey <span class="token operator">=</span> 0x0000000001420530
<span class="token punctuation">[</span>*<span class="token punctuation">]</span> patch_solution_since<span class="token operator">&lt;</span><span class="token number">16</span>, <span class="token number">0</span>, <span class="token number">7</span>, <span class="token operator"><span class="token file-descriptor important">0</span>&gt;</span>: m_va_pltgot_std_string_append <span class="token operator">=</span> 0x000000000306a608
<span class="token punctuation">[</span>+<span class="token punctuation">]</span> patch_solution_since<span class="token operator">&lt;</span><span class="token number">16</span>, <span class="token number">0</span>, <span class="token number">7</span>, <span class="token operator"><span class="token file-descriptor important">0</span>&gt;</span>: official encoded key is found.

<span class="token punctuation">[</span>*<span class="token punctuation">]</span> Generating new RSA private key, it may take a long time<span class="token punctuation">..</span>.
<span class="token punctuation">[</span>*<span class="token punctuation">]</span> Your RSA private key:
-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAnqeNFqE0n6k+Ys58R+IzIULlZ9oxasJns46vaVcnd9e9mTj4
hf/ArQohP5bex0UD+NGCLfQWKohnQxy9IFjQxZ6wUJnKOaA1UfdRr0ck7LZz5YV2
5CkF4VZ7UWGEp/LEQiJnIBAtp7Zq5PNviqI03PZv2MfZt1/6YstCvi8s0rmpAyTw
V5pteDipsI3lTapysLLsL+kQuJ0Z64GGk5D7rM8UPBt/Wjpe/qb/OwJKJ3Vi65/Z
RRLVt46euwdiW8ORmNt552zOdvQPgYLEP38dMCpoeqIwv7IFWWJHxt0JgOTmxZ5c
hjMq9ns3wyyEFjvSGSpM6sGL5eR1TMDQt8PSIwIDAQABAoIBAAp+jL+VdDSnbj/8
5o2escEeeqwu65vjNhbTdljicfLka18qPI4oh6cqh158bUoDD6syuIivn5O6qBHx
YbU1CsI3p/P86Dp9lWlRka6lZxTdULc6581ZxVDNdqTAbZTqYv745ZdiWpLAZzuz
uooSBqsjBezx8z3E9Hv6c/S+jBl4IcKx7vEgKbC47CAAnFZFGonRPBj88fXn7VmP
KEa7EVIE8yNG/xO4H+hlpio8XsTfbXB4Tx/Mw4f0ZCifFFE9aC503usbTQ9HCRn/
13VgBeFzC+uxluEvpm7r3efcI1vFLDHiKlAzO85vDhTOef3jZv29PTUwbkNCP8AM
CHP9abECgYEAzKWmGRcMVhwKL2RIc0VW1mSW82VTnjuTe9wnonYOGHmxCxQBZpB2
j8mjfPBNzg3pjhRryrmfA2V/1Rxk7/hnw9I+OnRJA2p+USOKVlXi5YA3CMtPcevS
DJaawPIkvjnnApSQ0CNYYKCgB43l33GJQ+btvYQiB/pmPwCiPhdsdYkCgYEAxndh
GVSRd9yHez9LggsTfS4uo1OlZReHrtyX933eEG4YtiwM90Mb/9ZCA6CY5/o8YpOp
AhPu7he3f67cQ7dotjilFHWqi4f+53d8NhfweQhM0azA9zzXjKhsao9jPjtTo+DW
BptWVNZOqv1v0Np4/BZ3rTKtP42vSR4/Ql7Mi0sCgYATzoiH7yIjh2047wTQG0rv
TycJAaqZKvz4RPOVFsYAem63OsVz7tF60zI+mmd9ZP1Q4gsYwORyCLXZo3jlfO5W
FpgtQin66ai2I7F077UZL1KkSEE1LnTTARSTThxeSO5h4o0th+460/EJKiOwf6Wg
a85gxFQi34pb2KzbQ5scuQKBgGLHA9LBnm0Tm3Kh/AjLTnXdSGUNuqHn5iYHsLMD
OEThJvd0UTe3dPYOQ2jew3uhtfAyIcng9egWccPg2cvyOvGGm9LlBW7QzvORKocZ
vxveH62z1461/2oIYX1fxDsy99v2iU9cfMlYqGq+HKrMMa7117aiJEwfToCLx1xX
JmKlAoGAI8aOpSjyQ/xVUS1niXmwbKPkOJP7jI3yQ6YpUEnho/wpFw2/OjuVdXw2
tcYFSg1GjBvTigDeh385TsjDa862SYAslgqZBPN4jXP20xw1kRr9kAQq6bvadC+b
WpwZ8C5vNxwX5ccbxz3WwJhHtZvIQt0J8hOf3+BgcedHxa+bcTQ<span class="token operator">=</span>
-----END RSA PRIVATE KEY-----

<span class="token punctuation">[</span>*<span class="token punctuation">]</span> patch_solution_since<span class="token operator">&lt;</span><span class="token number">16</span>, <span class="token number">0</span>, <span class="token number">7</span>, <span class="token operator"><span class="token file-descriptor important">0</span>&gt;</span>: Patch has been done.
<span class="token punctuation">[</span>*<span class="token punctuation">]</span> New RSA-2048 private key has been saved to
 /home/doublesine/RegPrivateKey.pem

*******************************************************
*  PATCH HAS BEEN DONE SUCCESSFULLY<span class="token operator">!</span>*
*HAVE FUN AND ENJOY~ *
*******************************************************
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>1.将文件重新打包成AppImage：</p><p>例如：</p><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token function">wget</span> <span class="token string">&#39;https://github.com/AppImage/AppImageKit/releases/download/continuous/appimagetool-x86_64.AppImage&#39;</span>
<span class="token function">chmod</span> +x appimagetool-x86_64.AppImage
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div><p>也可以</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>yay -S appimagetool
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code>./appimagetool-x86_64.AppImage ~/navicat16-premium-cs-patched ~/navicat16-premium-cs-patched.AppImage
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><p>运行刚生成的AppImage：</p><p>chmod +x ~/navicat16-premium-cs-patched.AppImage<br> ~/navicat16-premium-cs-patched.AppImage</p><p>使用 navicat-keygen 来生成 序列号 和 激活码。</p><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code>Usage:
 navicat-keygen <span class="token operator">&lt;</span>--bin<span class="token operator">|</span>-text<span class="token operator">&gt;</span> <span class="token punctuation">[</span>--adv<span class="token punctuation">]</span> <span class="token operator">&lt;</span>RSA-2048 private key file<span class="token operator">&gt;</span>

 <span class="token operator">&lt;</span>--bin<span class="token operator">|</span>--text<span class="token operator">&gt;</span>  Specify <span class="token string">&quot;--bin&quot;</span> to generate <span class="token string">&quot;license_file&quot;</span> used by Navicat <span class="token number">11</span>.
  Specify <span class="token string">&quot;--text&quot;</span> to generate base64-encoded activation code.
  This parameter is mandatory.

 <span class="token punctuation">[</span>--adv<span class="token punctuation">]</span>Enable advance mode.
  This parameter is optional.

 <span class="token operator">&lt;</span>RSA-2048 private key file<span class="token operator">&gt;</span> A path to an RSA-2048 private key file.
  This parameter is mandatory.
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>例如：</p><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code>./navicat-keygen <span class="token parameter variable">--text</span> ./RegPrivateKey.pem
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><p>你会被要求选择Navicat产品类别、Navicat语言版本和填写主版本号。之后一个随机生成的 序列号 将会给出。</p><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code>***************************************************
* navicat-keygen by @DoubleLabyrinth  *
* version: <span class="token number">16.0</span>.7.0 *
***************************************************

<span class="token punctuation">[</span>*<span class="token punctuation">]</span> Select Navicat product:
 <span class="token number">1</span>. DataModeler
 <span class="token number">2</span>. Premium
 <span class="token number">3</span>. MySQL
 <span class="token number">4</span>. PostgreSQL
 <span class="token number">5</span>. Oracle
 <span class="token number">6</span>. SQLServer
 <span class="token number">7</span>. SQLite
 <span class="token number">8</span>. MariaDB
 <span class="token number">9</span>. MongoDB
 <span class="token number">10</span>. ReportViewer

<span class="token punctuation">(</span>Input index<span class="token punctuation">)</span><span class="token operator">&gt;</span> <span class="token number">1</span>

<span class="token punctuation">[</span>*<span class="token punctuation">]</span> Select product language:
 <span class="token number">0</span>. English
 <span class="token number">1</span>. Simplified Chinese
 <span class="token number">2</span>. Traditional Chinese
 <span class="token number">3</span>. Japanese
 <span class="token number">4</span>. Polish
 <span class="token number">5</span>. Spanish
 <span class="token number">6</span>. French
 <span class="token number">7</span>. German
 <span class="token number">8</span>. Korean
 <span class="token number">9</span>. Russian
 <span class="token number">10</span>. Portuguese

<span class="token punctuation">(</span>Input index<span class="token punctuation">)</span><span class="token operator">&gt;</span> <span class="token number">0</span>

<span class="token punctuation">[</span>*<span class="token punctuation">]</span> Input major version number:
<span class="token punctuation">(</span>range: <span class="token number">11</span> ~ <span class="token number">16</span>, default: <span class="token number">16</span><span class="token punctuation">)</span><span class="token operator">&gt;</span> <span class="token number">16</span>

<span class="token punctuation">[</span>*<span class="token punctuation">]</span> Serial number:
NAVB-EZF4-7T7X-9MPG

<span class="token punctuation">[</span>*<span class="token punctuation">]</span> Your name:

你可以使用这个 序列号 来暂时激活Navicat。

之后你会被要求填写 用户名 和 组织名。你可以随意填写，但别太长。

<span class="token punctuation">[</span>*<span class="token punctuation">]</span> Your name: Double Sine
<span class="token punctuation">[</span>*<span class="token punctuation">]</span> Your organization: PremiumSoft CyberTech Ltd.

<span class="token punctuation">[</span>*<span class="token punctuation">]</span> Input request code <span class="token keyword">in</span> Base64: <span class="token punctuation">(</span>Double press ENTER to end<span class="token punctuation">)</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>之后你会被要求填写请求码。注意不要关闭keygen。</p><ol><li><p>断开网络. 找到注册窗口，填写keygen给你的 序列号，然后点击 激活。</p></li><li><p>通常在线激活会失败，所以在弹出的提示中选择 手动激活。</p></li><li><p>复制 请求码 到keygen，连按两次回车结束。</p></li></ol><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code> <span class="token punctuation">[</span>*<span class="token punctuation">]</span> Input request code <span class="token keyword">in</span> Base64: <span class="token punctuation">(</span>Double press ENTER to end<span class="token punctuation">)</span>
 ds7CnjEnNL+8Rme9Q5iD+3t9Tfuq9W6FzVN/3UZwC5zzecmM9EwyHJuZSovKJNSBTzL6AiGyxliTuKPWmLqAdwiKGLuD+mSaZ0syk0jTakVbXmbAk9maFkTz8SK5jMwnQVM/WBZcI0z2Jg1GnOCZVClu/Lo3/WF+XncS+alc2gshG9dUaI44Cqfvp/u1/EYso5fX/bjeBXaFW1/zj+uuRjVv5l0gt7JsTh9byGVxSDTO4zI64Iz9+58QYCbI9zKM+3G9Gou0UlNKjDYw4gN5+4dpiWAjitVTcL3oQzvflgAXjGlT/P6MA+8Xb5PEPJrEdxsErJObxBhO4cTH52wKoQ<span class="token operator">==</span>

 <span class="token punctuation">[</span>*<span class="token punctuation">]</span> Request Info:
 <span class="token punctuation">{</span><span class="token string">&quot;K&quot;</span><span class="token builtin class-name">:</span><span class="token string">&quot;NAVBEZF47T7X9MPG&quot;</span>, <span class="token string">&quot;DI&quot;</span><span class="token builtin class-name">:</span><span class="token string">&quot;AFCFB038A240942D8776&quot;</span>, <span class="token string">&quot;P&quot;</span><span class="token builtin class-name">:</span><span class="token string">&quot;linux&quot;</span><span class="token punctuation">}</span>

 <span class="token punctuation">[</span>*<span class="token punctuation">]</span> Response Info:
 <span class="token punctuation">{</span><span class="token string">&quot;K&quot;</span><span class="token builtin class-name">:</span><span class="token string">&quot;NAVBEZF47T7X9MPG&quot;</span>,<span class="token string">&quot;DI&quot;</span><span class="token builtin class-name">:</span><span class="token string">&quot;AFCFB038A240942D8776&quot;</span>,<span class="token string">&quot;N&quot;</span><span class="token builtin class-name">:</span><span class="token string">&quot;Double Sine&quot;</span>,<span class="token string">&quot;O&quot;</span><span class="token builtin class-name">:</span><span class="token string">&quot;PremiumSoft CyberTech Ltd.&quot;</span>,<span class="token string">&quot;T&quot;</span>:1644837835<span class="token punctuation">}</span>

 <span class="token punctuation">[</span>*<span class="token punctuation">]</span> Activation Code:
 OY8Ib0brsepeS99it4s4WTDPQuKgu93WembLJ0bzr6M30Wh24reH1/ocaZ2Ek1bRBi5lqu2xBv/MpAcFUlstJANtavArkFnXYv0ZZiF3VF70De5GMe/VjkreNhjCGtTZcQKr8fabBTPjJuN0P+Hi1xWwMs9zJMuH+MJTmCQpbM4gu86YrFK/EDcdHtA4ZFgUI0SgYW8lwFausLFHp7C4uIQNbjtv4KP3XolDUrAx4lqg6bklgZ9C8ZjUpg28VVR9Ym37b1Fup7Y7C8OjmmMiAp8N5z8m6cA/EjcSLfLOMGf8jsAK0GHz5/AGUqAXWifv9h9cxPA35UgytqI9F2IH/Q<span class="token operator">==</span>

</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ol start="4"><li>最终你会得到一个base64编码的 激活码。将之复制到 手动激活 的窗口，然后点击 激活。</li></ol><p>如果没有什么意外，应该可以成功激活。</p><p>最后清理：（可选步骤）</p><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token function">rm</span> ~/navicat16-premium-cs.AppImage
<span class="token function">rm</span> <span class="token parameter variable">-rf</span> ~/navicat16-premium-cs-patched
<span class="token function">mv</span> ~/navicat16-premium-cs-patched.AppImage ~/navicat16-premium-cs.AppImage
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,31),p=[t];function l(c,o){return s(),a("div",null,p)}const d=n(i,[["render",l],["__file","install-navciat.html.vue"]]);export{d as default};
