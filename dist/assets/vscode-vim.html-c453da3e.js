import{_ as n}from"./plugin-vue_export-helper-c27b6911.js";import{o as s,c as a,b as t}from"./app-af62e8e7.js";const e={},o=t(`<h1 id="vscode-中的vim" tabindex="-1"><a class="header-anchor" href="#vscode-中的vim" aria-hidden="true">#</a> vscode 中的vim</h1><h2 id="实现类似autocmd在insertleave时-如果是中文输入法则切换回英文输入法" tabindex="-1"><a class="header-anchor" href="#实现类似autocmd在insertleave时-如果是中文输入法则切换回英文输入法" aria-hidden="true">#</a> 实现类似autocmd在InsertLeave时，如果是中文输入法则切换回英文输入法</h2><blockquote><p>可能只适用于linux<br> 需要两个插件,</p></blockquote><ul><li>multi-command, 用于一次键绑定执行多条命令</li><li>Command Runner, 用于执行terminal 命令</li></ul><p>需要写好im_change函数</p><blockquote><p>im_change</p></blockquote><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token comment"># fcitx5 如果是中文输入法执行这个函数就会切换到英文输入法，给vim用</span>
<span class="token keyword">function</span> <span class="token function-name function">im_change</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">{</span>
  <span class="token assign-left variable">im</span><span class="token operator">=</span><span class="token variable"><span class="token variable">$(</span>fcitx5-remote<span class="token variable">)</span></span>
  <span class="token keyword">if</span> <span class="token punctuation">[</span> <span class="token variable">$im</span> <span class="token parameter variable">-eq</span> <span class="token number">2</span> <span class="token punctuation">]</span><span class="token punctuation">;</span> <span class="token keyword">then</span>
    fcitx5-remote <span class="token parameter variable">-c</span>
  <span class="token keyword">fi</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><blockquote><p>Comamnd Runner 配置</p></blockquote><div class="language-json line-numbers-mode" data-ext="json"><pre class="language-json"><code><span class="token property">&quot;command-runner.terminal.name&quot;</span><span class="token operator">:</span> <span class="token string">&quot;runCommand&quot;</span><span class="token punctuation">,</span>
<span class="token property">&quot;command-runner.terminal.autoClear&quot;</span><span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span>
<span class="token property">&quot;command-runner.terminal.autoFocus&quot;</span><span class="token operator">:</span> <span class="token boolean">false</span><span class="token punctuation">,</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><blockquote><p>keybindings.json</p></blockquote><div class="language-json line-numbers-mode" data-ext="json"><pre class="language-json"><code>  <span class="token punctuation">{</span>
    <span class="token property">&quot;key&quot;</span><span class="token operator">:</span> <span class="token string">&quot;ESCAPE&quot;</span><span class="token punctuation">,</span>
    <span class="token property">&quot;command&quot;</span><span class="token operator">:</span> <span class="token string">&quot;extension.multiCommand.execute&quot;</span><span class="token punctuation">,</span>
    <span class="token property">&quot;args&quot;</span><span class="token operator">:</span> <span class="token punctuation">{</span>
      <span class="token property">&quot;sequence&quot;</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token punctuation">{</span>
          <span class="token property">&quot;command&quot;</span><span class="token operator">:</span> <span class="token string">&quot;command-runner.run&quot;</span><span class="token punctuation">,</span>
          <span class="token property">&quot;args&quot;</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">&quot;command&quot;</span><span class="token operator">:</span> <span class="token string">&quot;im_change&quot;</span><span class="token punctuation">,</span>
          <span class="token punctuation">}</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span>
          <span class="token property">&quot;command&quot;</span><span class="token operator">:</span> <span class="token string">&quot;extension.vim_escape&quot;</span>
        <span class="token punctuation">}</span>
      <span class="token punctuation">]</span>
    <span class="token punctuation">}</span>
  <span class="token punctuation">}</span><span class="token punctuation">,</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="模仿vim-tree的行为" tabindex="-1"><a class="header-anchor" href="#模仿vim-tree的行为" aria-hidden="true">#</a> 模仿vim-tree的行为</h2><blockquote><p>keybindings.json</p></blockquote><p>需要一个插件，multiCommand, 这个插件可以让你一次键绑定执行多条命令</p><div class="language-json line-numbers-mode" data-ext="json"><pre class="language-json"><code><span class="token punctuation">{</span>
  <span class="token property">&quot;key&quot;</span><span class="token operator">:</span> <span class="token string">&quot;space e&quot;</span><span class="token punctuation">,</span>
  <span class="token property">&quot;command&quot;</span><span class="token operator">:</span> <span class="token string">&quot;extension.multiCommand.execute&quot;</span><span class="token punctuation">,</span>
  <span class="token property">&quot;args&quot;</span><span class="token operator">:</span> <span class="token punctuation">{</span>
    <span class="token property">&quot;sequence&quot;</span><span class="token operator">:</span> <span class="token punctuation">[</span>
      <span class="token string">&quot;workbench.action.toggleSidebarVisibility&quot;</span><span class="token punctuation">,</span>
      <span class="token string">&quot;workbench.action.focusSideBar&quot;</span><span class="token punctuation">,</span>
    <span class="token punctuation">]</span>
  <span class="token punctuation">}</span><span class="token punctuation">,</span>
  <span class="token property">&quot;when&quot;</span><span class="token operator">:</span> <span class="token string">&quot;!sideBarFocus &amp;&amp; !sideBarVisible &amp;&amp; vim.mode == &#39;Normal&#39; &amp;&amp; !terminalFocus || inWelcome&quot;</span>
<span class="token punctuation">}</span><span class="token punctuation">,</span>
<span class="token punctuation">{</span>
  <span class="token property">&quot;key&quot;</span><span class="token operator">:</span> <span class="token string">&quot;space e&quot;</span><span class="token punctuation">,</span>
  <span class="token property">&quot;command&quot;</span><span class="token operator">:</span> <span class="token string">&quot;workbench.action.toggleSidebarVisibility&quot;</span><span class="token punctuation">,</span>
  <span class="token property">&quot;when&quot;</span><span class="token operator">:</span> <span class="token string">&quot;sideBarVisible &amp;&amp; vim.mode == &#39;Normal&#39; &amp;&amp; !terminalFocus || inWelcome&quot;</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="我自己的keybingings配置" tabindex="-1"><a class="header-anchor" href="#我自己的keybingings配置" aria-hidden="true">#</a> 我自己的keybingings配置</h2><div class="language-json line-numbers-mode" data-ext="json"><pre class="language-json"><code><span class="token comment">// 将键绑定放在此文件中以覆盖默认值auto[]</span>
<span class="token comment">// 将键绑定放在此文件中以覆盖默认值auto[]</span>
<span class="token punctuation">[</span>
  <span class="token punctuation">{</span>
    <span class="token property">&quot;key&quot;</span><span class="token operator">:</span> <span class="token string">&quot;space e&quot;</span><span class="token punctuation">,</span>
    <span class="token property">&quot;command&quot;</span><span class="token operator">:</span> <span class="token string">&quot;extension.multiCommand.execute&quot;</span><span class="token punctuation">,</span>
    <span class="token property">&quot;args&quot;</span><span class="token operator">:</span> <span class="token punctuation">{</span>
      <span class="token property">&quot;sequence&quot;</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">&quot;workbench.action.toggleSidebarVisibility&quot;</span><span class="token punctuation">,</span>
        <span class="token string">&quot;workbench.action.focusSideBar&quot;</span><span class="token punctuation">,</span>
      <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token property">&quot;when&quot;</span><span class="token operator">:</span> <span class="token string">&quot;!sideBarFocus &amp;&amp; !sideBarVisible &amp;&amp; vim.mode == &#39;Normal&#39; &amp;&amp; !terminalFocus || inWelcome&quot;</span>
  <span class="token punctuation">}</span><span class="token punctuation">,</span>
  <span class="token punctuation">{</span>
    <span class="token property">&quot;key&quot;</span><span class="token operator">:</span> <span class="token string">&quot;space e&quot;</span><span class="token punctuation">,</span>
    <span class="token property">&quot;command&quot;</span><span class="token operator">:</span> <span class="token string">&quot;workbench.action.toggleSidebarVisibility&quot;</span><span class="token punctuation">,</span>
    <span class="token property">&quot;when&quot;</span><span class="token operator">:</span> <span class="token string">&quot;sideBarVisible &amp;&amp; vim.mode == &#39;Normal&#39; &amp;&amp; !terminalFocus || inWelcome&quot;</span>
  <span class="token punctuation">}</span><span class="token punctuation">,</span>
  <span class="token punctuation">{</span>
    <span class="token property">&quot;key&quot;</span><span class="token operator">:</span> <span class="token string">&quot;space f&quot;</span><span class="token punctuation">,</span>
    <span class="token property">&quot;command&quot;</span><span class="token operator">:</span> <span class="token string">&quot;workbench.action.quickOpen&quot;</span><span class="token punctuation">,</span>
    <span class="token property">&quot;when&quot;</span><span class="token operator">:</span> <span class="token string">&quot;vim.mode == &#39;Normal&#39; &amp;&amp; !terminalFocus || inWelcome&quot;</span>
  <span class="token punctuation">}</span><span class="token punctuation">,</span>
  <span class="token punctuation">{</span>
    <span class="token property">&quot;key&quot;</span><span class="token operator">:</span> <span class="token string">&quot;space r&quot;</span><span class="token punctuation">,</span>
    <span class="token property">&quot;command&quot;</span><span class="token operator">:</span> <span class="token string">&quot;workbench.action.openRecent&quot;</span><span class="token punctuation">,</span>
    <span class="token property">&quot;when&quot;</span><span class="token operator">:</span> <span class="token string">&quot;vim.mode == &#39;Normal&#39; &amp;&amp; !terminalFocus || inWelcome&quot;</span>
  <span class="token punctuation">}</span><span class="token punctuation">,</span>
  
  <span class="token punctuation">{</span>
    <span class="token property">&quot;key&quot;</span><span class="token operator">:</span> <span class="token string">&quot;ESCAPE&quot;</span><span class="token punctuation">,</span>
    <span class="token property">&quot;command&quot;</span><span class="token operator">:</span> <span class="token string">&quot;extension.multiCommand.execute&quot;</span><span class="token punctuation">,</span>
    <span class="token property">&quot;args&quot;</span><span class="token operator">:</span> <span class="token punctuation">{</span>
      <span class="token property">&quot;sequence&quot;</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token punctuation">{</span>
          <span class="token property">&quot;command&quot;</span><span class="token operator">:</span> <span class="token string">&quot;command-runner.run&quot;</span><span class="token punctuation">,</span>
          <span class="token property">&quot;args&quot;</span><span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token property">&quot;command&quot;</span><span class="token operator">:</span> <span class="token string">&quot;im_change&quot;</span><span class="token punctuation">,</span>
          <span class="token punctuation">}</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        <span class="token punctuation">{</span>
          <span class="token property">&quot;command&quot;</span><span class="token operator">:</span> <span class="token string">&quot;extension.vim_escape&quot;</span>
        <span class="token punctuation">}</span>
      <span class="token punctuation">]</span>
    <span class="token punctuation">}</span>
  <span class="token punctuation">}</span><span class="token punctuation">,</span>
<span class="token punctuation">]</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="我的keybindings里面写着space-e-这样的所以vim-leader-只好使用-了" tabindex="-1"><a class="header-anchor" href="#我的keybindings里面写着space-e-这样的所以vim-leader-只好使用-了" aria-hidden="true">#</a> 我的keybindings里面写着space e,这样的所以vim leader 只好使用;了</h2><h2 id="使用h-l向左-右切换tab" tabindex="-1"><a class="header-anchor" href="#使用h-l向左-右切换tab" aria-hidden="true">#</a> 使用H,L向左，右切换tab</h2><div class="language-json line-numbers-mode" data-ext="json"><pre class="language-json"><code><span class="token property">&quot;vim.normalModeKeyBindingsNonRecursive&quot;</span><span class="token operator">:</span> <span class="token punctuation">[</span>
    <span class="token punctuation">{</span>
      <span class="token property">&quot;before&quot;</span><span class="token operator">:</span> <span class="token punctuation">[</span><span class="token string">&quot;p&quot;</span><span class="token punctuation">]</span><span class="token punctuation">,</span>
      <span class="token property">&quot;after&quot;</span><span class="token operator">:</span> <span class="token punctuation">[</span><span class="token string">&quot;p&quot;</span><span class="token punctuation">,</span><span class="token string">&quot;g&quot;</span><span class="token punctuation">,</span><span class="token string">&quot;v&quot;</span><span class="token punctuation">,</span><span class="token string">&quot;y&quot;</span><span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
      <span class="token property">&quot;before&quot;</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">&quot;&lt;S-h&gt;&quot;</span>
      <span class="token punctuation">]</span><span class="token punctuation">,</span>
      <span class="token property">&quot;commands&quot;</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">&quot;:tabp&quot;</span>
      <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
    <span class="token punctuation">{</span>
      <span class="token property">&quot;before&quot;</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">&quot;&lt;S-l&gt;&quot;</span>
      <span class="token punctuation">]</span><span class="token punctuation">,</span>
      <span class="token property">&quot;commands&quot;</span><span class="token operator">:</span> <span class="token punctuation">[</span>
        <span class="token string">&quot;:tabn&quot;</span>
      <span class="token punctuation">]</span>
    <span class="token punctuation">}</span><span class="token punctuation">,</span>
  <span class="token punctuation">]</span><span class="token punctuation">,</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,20),p=[o];function i(c,l){return s(),a("div",null,p)}const d=n(e,[["render",i],["__file","vscode-vim.html.vue"]]);export{d as default};
