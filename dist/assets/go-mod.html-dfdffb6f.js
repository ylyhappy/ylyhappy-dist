import{_ as n}from"./plugin-vue_export-helper-c27b6911.js";import{o as a,c as s,b as e}from"./app-af62e8e7.js";const i={},o=e(`<h1 id="为什么使用-go-mod" tabindex="-1"><a class="header-anchor" href="#为什么使用-go-mod" aria-hidden="true">#</a> 为什么使用 go mod</h1><p>我忘了（hhh）,反正用就对了</p><h2 id="使用go-mod-创建一个项目" tabindex="-1"><a class="header-anchor" href="#使用go-mod-创建一个项目" aria-hidden="true">#</a> 使用go mod 创建一个项目</h2><p>ProjectName 可以是你的github仓库地址(recommand) 或者是你喜欢的名字<br><strong>不要使用你的本地文件路径，会报错</strong><br> e.g go mod init hello<br> 你的文件目录结构</p><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token builtin class-name">.</span>
├── api
│   └── api.go
├── go.mod
└── main.go
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p><strong>main.go</strong></p><div class="language-go line-numbers-mode" data-ext="go"><pre class="language-go"><code><span class="token keyword">package</span> main
<span class="token keyword">import</span> <span class="token punctuation">(</span>
  <span class="token string">&quot;hhh/api&quot;</span>
<span class="token punctuation">)</span>
<span class="token keyword">func</span> <span class="token function">main</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">{</span>
  api<span class="token punctuation">.</span><span class="token function">HelloWorld</span><span class="token punctuation">(</span><span class="token punctuation">)</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>go mod init [ProjectName]</p><h2 id="使用go-mod-安装依赖" tabindex="-1"><a class="header-anchor" href="#使用go-mod-安装依赖" aria-hidden="true">#</a> 使用go mod 安装依赖</h2><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code>go mod tidy
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><h2 id="使用go-mod-打包成二进制文件" tabindex="-1"><a class="header-anchor" href="#使用go-mod-打包成二进制文件" aria-hidden="true">#</a> 使用go mod 打包成二进制文件</h2><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code>go build main.go
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div>`,12),d=[o];function l(c,t){return a(),s("div",null,d)}const p=n(i,[["render",l],["__file","go-mod.html.vue"]]);export{p as default};
