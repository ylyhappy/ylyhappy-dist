import{_ as i}from"./plugin-vue_export-helper-c27b6911.js";import{o as n,c as e,b as t}from"./app-af62e8e7.js";const l={},d=t(`<h1 id="thread-join-是什么" tabindex="-1"><a class="header-anchor" href="#thread-join-是什么" aria-hidden="true">#</a> thread.join 是什么</h1><p>main.cpp</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>#include &lt;chrono&gt;
#include &lt;ctime&gt;
#include &lt;iostream&gt;
#include &lt;thread&gt;

using namespace std;

string getCurrentTimeStr() {
  time_t t = time(0);
  char timeStr[128];
  strftime(timeStr, sizeof(timeStr), &quot;%H:%M:%S&quot;, localtime(&amp;t));
  return timeStr;
}

void print0() {
  this_thread::sleep_for(chrono::milliseconds(5000));
  cout &lt;&lt; getCurrentTimeStr() &lt;&lt; &quot; &quot; &lt;&lt; 0 &lt;&lt; endl;
}

void print1() {
  this_thread::sleep_for(chrono::milliseconds(5000));
  cout &lt;&lt; getCurrentTimeStr() &lt;&lt; &quot; &quot; &lt;&lt; 1 &lt;&lt; endl;
}

void print2() {
  this_thread::sleep_for(chrono::milliseconds(5000));
  cout &lt;&lt; getCurrentTimeStr() &lt;&lt; &quot; &quot; &lt;&lt; 2 &lt;&lt; endl;
}

int main(int argc, char *argv[]) {
  cout &lt;&lt; getCurrentTimeStr() &lt;&lt; &quot; &quot; &lt;&lt; &quot;init&quot; &lt;&lt; endl;
  thread t1(print1);
  thread t2(print2);
  t1.join();
  cout &lt;&lt; &quot;join 没有阻塞&quot; &lt;&lt; endl;
  print0();
  cout &lt;&lt; &quot;join 没有阻塞&quot; &lt;&lt; endl;
  t2.join();
  return 0;
}
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>t1, t2会同时执行完<br> 输出结果：<br> 18:12:16 1<br> join 没有阻塞<br> 18:12:16 2<br> 18:12:21 0<br> join 没有阻塞</p><h3 id="这个例子-第二个-join-没有阻塞-输出时机不理解" tabindex="-1"><a class="header-anchor" href="#这个例子-第二个-join-没有阻塞-输出时机不理解" aria-hidden="true">#</a> 这个例子，第二个（join 没有阻塞）输出时机不理解</h3><h3 id="这个例子-t1-join-居然没有阻塞t2-join" tabindex="-1"><a class="header-anchor" href="#这个例子-t1-join-居然没有阻塞t2-join" aria-hidden="true">#</a> 这个例子，t1.join（），居然没有阻塞t2.join();</h3><h3 id="t1-t2同时join的。这里就很奇怪" tabindex="-1"><a class="header-anchor" href="#t1-t2同时join的。这里就很奇怪" aria-hidden="true">#</a> t1,t2同时join的。这里就很奇怪</h3><h2 id="detach-类似join。-但是datach会先执行。" tabindex="-1"><a class="header-anchor" href="#detach-类似join。-但是datach会先执行。" aria-hidden="true">#</a> detach, 类似join。 但是datach会先执行。</h2><h2 id="不能double-join。-会出错误-join之前-先-test-joinable" tabindex="-1"><a class="header-anchor" href="#不能double-join。-会出错误-join之前-先-test-joinable" aria-hidden="true">#</a> 不能double join。 会出错误, join之前， 先 test joinable</h2>`,9),r=[d];function a(s,o){return n(),e("div",null,r)}const v=i(l,[["render",a],["__file","thread.html.vue"]]);export{v as default};
