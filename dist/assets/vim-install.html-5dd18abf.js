import{_ as a}from"./plugin-vue_export-helper-c27b6911.js";import{o as e,c as n,b as s}from"./app-af62e8e7.js";const i={},l=s(`<h1 id="vim-配置" tabindex="-1"><a class="header-anchor" href="#vim-配置" aria-hidden="true">#</a> vim 配置</h1><h3 id="安装lua" tabindex="-1"><a class="header-anchor" href="#安装lua" aria-hidden="true">#</a> 安装lua</h3><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token function">curl</span> <span class="token parameter variable">-R</span> <span class="token parameter variable">-O</span> http://www.lua.org/ftp/lua-5.4.4.tar.gz
<span class="token function">tar</span> zxf lua-5.4.4.tar.gz
<span class="token builtin class-name">cd</span> lua-5.4.4
<span class="token function">make</span> all <span class="token builtin class-name">test</span>
<span class="token function">make</span> insatll

</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="安装neovim" tabindex="-1"><a class="header-anchor" href="#安装neovim" aria-hidden="true">#</a> 安装neovim</h3><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code>yay <span class="token parameter variable">-S</span> neovim
yay <span class="token parameter variable">-S</span> nvim-packer
<span class="token function">git</span> clone https://gitee.com/ylyhappy/dotfile
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,5),t=[l];function r(c,d){return e(),n("div",null,t)}const u=a(i,[["render",r],["__file","vim-install.html.vue"]]);export{u as default};
