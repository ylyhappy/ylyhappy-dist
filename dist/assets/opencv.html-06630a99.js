import{_ as e}from"./plugin-vue_export-helper-c27b6911.js";import{o as n,c as i,b as a}from"./app-af62e8e7.js";const d={},s=a(`<h1 id="opencv-安装" tabindex="-1"><a class="header-anchor" href="#opencv-安装" aria-hidden="true">#</a> OpenCV 安装</h1><ol><li>安装需要的组件</li></ol><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>sudo pacman -S cuda cudnn opencv-cuda 
sudo pacman -S vtk hdf5 glew
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div><ol start="2"><li>cmake使用<br> CMakeLists.txt</li></ol><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>cmake_minimu_version(xxx)
project(testOpenCV)
set(CMAKE_EXPORT_COMPILE_COMMANDS True)
find_package(OpenCV REQUIED)
include_directories(\${OpenCV_INCLUDE_DIRS})
add_executable(main.cpp \${PROJECT_NAME})
target_link_libraries(\${PROJECT_NAME} \${OpenCV_LIBRARIES})
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,5),c=[s];function l(t,r){return n(),i("div",null,c)}const u=e(d,[["render",l],["__file","opencv.html.vue"]]);export{u as default};
