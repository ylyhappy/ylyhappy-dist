import{_ as n}from"./plugin-vue_export-helper-c27b6911.js";import{o as s,c as a,b as e}from"./app-af62e8e7.js";const t="/assets/img/2022/12/25/typescipt-note1.png",p={},o=e('<h1 id="typescipt-笔记" tabindex="-1"><a class="header-anchor" href="#typescipt-笔记" aria-hidden="true">#</a> TypeScipt 笔记</h1><h3 id="如何获取传入函数参数的类型" tabindex="-1"><a class="header-anchor" href="#如何获取传入函数参数的类型" aria-hidden="true">#</a> 如何获取传入函数参数的类型</h3><p>可以使用<code>泛型参数</code>获取， 我也不知道为什么这样可以</p><p>这个例子中form，是能获取到类型的</p><figure><img src="'+t+`" alt="form类型" tabindex="0" loading="lazy"><figcaption>form类型</figcaption></figure><div class="language-typescript line-numbers-mode" data-ext="ts"><pre class="language-typescript"><code><span class="token keyword">function</span> <span class="token generic-function"><span class="token function">submit</span><span class="token generic class-name"><span class="token operator">&lt;</span><span class="token constant">T</span> <span class="token keyword">extends</span> Record<span class="token operator">&lt;</span><span class="token builtin">string</span><span class="token punctuation">,</span> <span class="token builtin">any</span><span class="token operator">&gt;&gt;</span></span></span><span class="token punctuation">(</span>opt<span class="token operator">:</span> <span class="token punctuation">{</span>
  form<span class="token operator">:</span> <span class="token constant">T</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
  <span class="token keyword">return</span> <span class="token punctuation">{</span>
    form<span class="token operator">:</span> opt<span class="token punctuation">.</span>form
  <span class="token punctuation">}</span>
<span class="token punctuation">}</span>
<span class="token keyword">const</span> defaultForm <span class="token operator">=</span> <span class="token punctuation">{</span>
  a<span class="token operator">:</span> <span class="token number">1</span>
<span class="token punctuation">}</span>
<span class="token keyword">const</span> <span class="token punctuation">{</span>
  form
<span class="token punctuation">}</span> <span class="token operator">=</span> <span class="token function">submit</span><span class="token punctuation">(</span><span class="token punctuation">{</span>
  form<span class="token operator">:</span> <span class="token punctuation">{</span>
    a<span class="token operator">:</span> <span class="token number">1</span><span class="token punctuation">,</span>
    b<span class="token operator">:</span> <span class="token number">2</span>
  <span class="token punctuation">}</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>

</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,6),c=[o];function i(l,r){return s(),a("div",null,c)}const k=n(p,[["render",i],["__file","typescript-notes.html.vue"]]);export{k as default};
