import{_ as o}from"./plugin-vue_export-helper-c27b6911.js";import{r as t,o as p,c,e as s,f as n,d as a,w as r,b as i}from"./app-af62e8e7.js";const d={},v=i(`<h1 id="vsftpd的安装-配置和使用" tabindex="-1"><a class="header-anchor" href="#vsftpd的安装-配置和使用" aria-hidden="true">#</a> vsftpd的安装, 配置和使用</h1><h3 id="_0-安装vsftpd" tabindex="-1"><a class="header-anchor" href="#_0-安装vsftpd" aria-hidden="true">#</a> 0. 安装vsftpd,</h3><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token function">apt</span> <span class="token function">install</span> vsftpd
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><h3 id="_1-启动vsftpd-或者启用vsftpd" tabindex="-1"><a class="header-anchor" href="#_1-启动vsftpd-或者启用vsftpd" aria-hidden="true">#</a> 1. 启动vsftpd, 或者启用vsftpd</h3><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code>systemctl <span class="token builtin class-name">enable</span> vsftpd <span class="token parameter variable">--now</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div>`,5),u={id:"_2-设置本地用户访问模式-useradd-命令",tabindex:"-1"},m=s("a",{class:"header-anchor",href:"#_2-设置本地用户访问模式-useradd-命令","aria-hidden":"true"},"#",-1),b=i(`<p>创建一个ftp的用户，ftpuser1,</p><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token function">useradd</span> ftpuser1
password ftpuser1
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div><p>如果是个人偶尔使用，建议这个目录<br> mkdir $HOME/ftp/ -p<br> 服务器的话就<br> mkdir /var/ftp/ -p</p><h3 id="_3-直接上脚本" tabindex="-1"><a class="header-anchor" href="#_3-直接上脚本" aria-hidden="true">#</a> 3. 直接上脚本</h3><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token comment"># vsftpd配置文件位置</span>
<span class="token comment"># confPath=$1</span>
<span class="token assign-left variable">confPath</span><span class="token operator">=</span>/etc/vsftpd.conf
<span class="token comment"># 指定例外用户名单位置</span>
<span class="token comment"># chroot_list_file=$2</span>
<span class="token assign-left variable">chroot_list_file</span><span class="token operator">=</span>/etc/vsftpd/chroot_list
<span class="token builtin class-name">echo</span> <span class="token string">&quot;ftpuser1&quot;</span> <span class="token operator">&gt;</span> <span class="token variable">$chroot_list_file</span>
<span class="token comment"># 服务器公网ip</span>
<span class="token assign-left variable">ecs_addr</span><span class="token operator">=</span><span class="token variable">$3</span>
<span class="token comment"># 被动模式下，数据传输端口范围</span>
<span class="token comment"># 禁用匿名登陆</span>
<span class="token function">sed</span> <span class="token parameter variable">-i</span> <span class="token string">&#39;s/anonymouse_enable=YES/anonymouse_enable=NO/&#39;</span> <span class="token variable">$confPath</span>
<span class="token comment"># 打开监听ipv4 sockets</span>
<span class="token function">sed</span> <span class="token parameter variable">-i</span> <span class="token string">&#39;s/listen=NO/#listen=YES/&#39;</span> <span class="token variable">$confPath</span> 
<span class="token comment"># 关闭监听ipv6 sockets</span>
<span class="token function">sed</span> <span class="token parameter variable">-i</span> <span class="token string">&#39;s/listen_ipv6=YES/#listen_ipv6=YES/&#39;</span> <span class="token variable">$confPath</span> 
<span class="token comment"># 限制用户在给它分配的目录内</span>
<span class="token function">sed</span> <span class="token parameter variable">-i</span> <span class="token string">&#39;s/#chroot_local_user=YES/chroot_local_user=YES/&#39;</span> <span class="token variable">$confPath</span>
<span class="token comment"># 启用例外用户</span>
<span class="token function">sed</span> <span class="token parameter variable">-i</span> <span class="token string">&#39;s/#chroot_list_enable=YES/chroot_list_enable=YES/&#39;</span> <span class="token variable">$confPath</span>
<span class="token comment"># 指定例外用户名单位置</span>
<span class="token function">sed</span> <span class="token parameter variable">-i</span> <span class="token string">&#39;s/#chroot_list_file=/chroot_list_file=/&#39;</span> <span class="token variable">$confPath</span>

<span class="token comment"># 给予写权限在给它分配的目录内</span>
<span class="token builtin class-name">echo</span> <span class="token string">&quot;allow_writeable_chroot=YES&quot;</span> <span class="token operator">&gt;&gt;</span> <span class="token variable">$confPath</span>
<span class="token comment"># 开启被动模式</span>
<span class="token builtin class-name">echo</span> <span class="token string">&quot;pasv_enable=YES&quot;</span> <span class="token operator">&gt;&gt;</span> <span class="token variable">$confPath</span>
<span class="token comment"># 设置ip</span>
<span class="token builtin class-name">echo</span> <span class="token string">&quot;pasv_address=<span class="token variable">$ecs_addr</span>&quot;</span> <span class="token operator">&gt;&gt;</span> <span class="token variable">$confPath</span>
<span class="token comment"># 设置被动模式下，建立数据传输端口范围</span>
<span class="token builtin class-name">echo</span> <span class="token string">&quot;pasv_min_port=10040&quot;</span> <span class="token operator">&gt;&gt;</span> <span class="token variable">$confPath</span>
<span class="token builtin class-name">echo</span> <span class="token string">&quot;pasv_max_port=10050&quot;</span> <span class="token operator">&gt;&gt;</span> <span class="token variable">$confPath</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="_4-重启vsftpd-让修改的配置生效" tabindex="-1"><a class="header-anchor" href="#_4-重启vsftpd-让修改的配置生效" aria-hidden="true">#</a> 4. 重启vsftpd，让修改的配置生效</h3><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code>systemctl restart vsftpd
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><h3 id="_5-ftp-连接问题" tabindex="-1"><a class="header-anchor" href="#_5-ftp-连接问题" aria-hidden="true">#</a> 5. ftp 连接问题</h3><ol><li>在命令行中，连接ftp，默认是主动模式,不能使用ls,输入pass就可以切换到被动模式</li></ol><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code>ftp<span class="token operator">&gt;</span> <span class="token function">ls</span>
<span class="token number">500</span> Illegal PORT command.
<span class="token number">500</span> Unknown command.
ftp: bind: Address already <span class="token keyword">in</span> use
ftp<span class="token operator">&gt;</span> pass
Passive mode on.
ftp<span class="token operator">&gt;</span> <span class="token function">ls</span>
<span class="token number">227</span> Entering Passive Mode <span class="token punctuation">(</span><span class="token number">59,110</span>,160,188,39,64<span class="token punctuation">)</span>.
<span class="token number">150</span> Here comes the directory listing.
drwxr-xr-x    <span class="token number">2</span> <span class="token number">1000</span>     <span class="token number">100</span>          <span class="token number">4096</span> Nov <span class="token number">23</span> 09:38 ftpuser1
<span class="token number">226</span> Directory send OK.
ftp<span class="token operator">&gt;</span> 
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ol start="2"><li>卡在227 enter passive mode 不动了</li></ol><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token number">227</span> Entering Passive Mode <span class="token punctuation">(</span><span class="token number">59,110</span>,160,188,37,2<span class="token punctuation">)</span>.
ftp: connect: Connection timed out
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div><p>这种情况，大概率是pasv_min_port, pasv_max_port 这个设置不对。 需要大于1024。我设置成20,21 就会出现上面的错误。但是改成 10040, 10050就没有问题了。</p><ol start="3"><li>不能上传文件</li></ol><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token number">500</span> permission denied.
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><p>修改vsftpd的配置文件，写入write_enable=YES</p><h3 id="参考文章" tabindex="-1"><a class="header-anchor" href="#参考文章" aria-hidden="true">#</a> 参考文章</h3>`,17),h={href:"https://www.cnblogs.com/kuliuheng/p/3209744.html",target:"_blank",rel:"noopener noreferrer"},k={href:"https://www.cnblogs.com/ssrs-wanghao/articles/13751408.html",target:"_blank",rel:"noopener noreferrer"};function f(_,g){const l=t("RouterLink"),e=t("ExternalLinkIcon");return p(),c("div",null,[v,s("h3",u,[m,n(" 2. 设置本地用户访问模式("),a(l,{to:"/posts/linux/install_or_config/useradd.html"},{default:r(()=>[n("useradd 命令")]),_:1}),n(" )")]),b,s("ol",null,[s("li",null,[s("a",h,[n("vsftpd 被动模式"),a(e)])]),s("li",null,[s("a",k,[n("vsftpd 配置项"),a(e)])])])])}const E=o(d,[["render",f],["__file","install-ftp.html.vue"]]);export{E as default};
